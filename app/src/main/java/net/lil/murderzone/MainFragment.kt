package net.lil.murderzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.fragment.findNavController
import coil.load
import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.response
import it.skrape.fetcher.skrape
import net.lil.murderzone.databinding.FragmentBoardBinding
import net.lil.murderzone.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        skrape(HttpFetcher) {
            request { url = "http://murder.zone/" }
            response {
                htmlDocument(responseBody) {
                    binding.heading.text = findFirst(".head").text
                    binding.subheading.text = findFirst(".tag").text

                }
            }
        }

        val buttons = listOf(
            binding.genButton,
            binding.musButton,
            binding.diyButton,
            binding.imgButton,
            binding.devButton,
            binding.tstButton
        )

        for (but in buttons) {
            but.setOnClickListener {
                val action =
                    MainFragmentDirections.actionMainFragmentToBoardFragment(but.text.toString())
                this.findNavController().navigate(action)
            }
        }

        //todo images, bul, about, settings, rules, links
        // loading toast?
    }
}
