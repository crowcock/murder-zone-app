package net.lil.murderzone

import android.os.Build.BOARD
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import coil.load
import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.response
import it.skrape.fetcher.skrape
import net.lil.murderzone.databinding.FragmentBoardBinding
import org.jsoup.select.Collector.findFirst

class BoardFragment : Fragment() {

    private lateinit var binding: FragmentBoardBinding
    private lateinit var board: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            board = it.getString("board").toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentBoardBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        skrape(HttpFetcher) {
            request { url = "http://murder.zone/$board" }
            response {
                htmlDocument(responseBody) {

                    binding.bannerImage.load(
                        "http://murder.zone${findFirst(".banner").eachSrc[0]}"
                    )

                    binding.heading.text = findFirst(".head").text
                    binding.subheading.text = findFirst(".tag").text

                    val threads = findAll(".post")

                    binding.threadsRV.adapter = ThreadAdapter(threads)
                }
            }
        }

        binding.homeButton.setOnClickListener {
            val action = BoardFragmentDirections.actionBoardFragmentToMainFragment()
            this.findNavController().navigate(action)
        }
    }
}
