package net.lil.murderzone

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import it.skrape.selects.DocElement
import net.lil.murderzone.databinding.ThreadBinding

class ThreadAdapter(private val threads: List<DocElement>) :
    RecyclerView.Adapter<ThreadAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = ThreadBinding.inflate(LayoutInflater.from(viewGroup.context))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(threads[position])
    }

    override fun getItemCount() = threads.size

    class ViewHolder(private val binding: ThreadBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        lateinit var thread: String

        fun bind(element: DocElement) {
            binding.button.text = element.text

            thread = element.eachHref[0]

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            val action = BoardFragmentDirections.actionBoardFragmentToThreadFragment(thread)
            v.findNavController().navigate(action)
        }
    }
}
