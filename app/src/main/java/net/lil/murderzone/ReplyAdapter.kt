package net.lil.murderzone

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import it.skrape.selects.DocElement
import net.lil.murderzone.databinding.ReplyBinding

class ReplyAdapter(private val replies: List<DocElement>, private val threadPath: String) :
    RecyclerView.Adapter<ReplyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = ReplyBinding.inflate(LayoutInflater.from(viewGroup.context))
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(replies[position], threadPath)
    }

    override fun getItemCount() = replies.size

    class ViewHolder(private val binding: ReplyBinding) : RecyclerView.ViewHolder(binding.root),
        View.OnClickListener {

        fun bind(element: DocElement, threadPath: String) {
            binding.textView.text = element.text

            if (element.eachSrc.isNotEmpty()) {
                binding.imageView.load("http://murder.zone$threadPath${element.eachSrc[0]}")
                binding.imageView.visibility = View.VISIBLE
            }

            itemView.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            //todo copy # link
        }
    }
}
