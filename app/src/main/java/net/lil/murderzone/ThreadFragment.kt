package net.lil.murderzone

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import coil.load
import it.skrape.core.htmlDocument
import it.skrape.fetcher.HttpFetcher
import it.skrape.fetcher.response
import it.skrape.fetcher.skrape
import it.skrape.selects.DocElement
import net.lil.murderzone.databinding.FragmentThreadBinding
import it.skrape.selects.ElementNotFoundException
import org.jsoup.nodes.Element

class ThreadFragment : Fragment() {

    private lateinit var binding: FragmentThreadBinding
    private lateinit var threadPath: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            threadPath = it.getString("thread").toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        binding = FragmentThreadBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        skrape(HttpFetcher) {
            request { url = "http://murder.zone$threadPath" }
            response {
                htmlDocument(responseBody) {

                    binding.bannerImage.load(
                        "http://murder.zone${findFirst(".banner").eachSrc[0]}"
                    )

                    binding.heading.text = findFirst(".head").text

                    var hasAnyImages = true
                    var uploadImageElement: DocElement? = null

                    try {
                        uploadImageElement = findFirst(".upload")
                    } catch (e: ElementNotFoundException) {
                        hasAnyImages = false
                    }

                    if (hasAnyImages && uploadImageElement!!.parent.hasClass("page")) {
                        binding.headImage.load(
                            "http://murder.zone$threadPath${uploadImageElement.eachSrc[0]}"
                        )
                        binding.headImage.visibility = View.VISIBLE
                    }

                    val replies = findAll(".post")

                    binding.repliesRV.adapter = ReplyAdapter(replies, threadPath)
                }
            }
        }

        binding.backButton.setOnClickListener {
            val action =
                ThreadFragmentDirections.actionThreadFragmentToBoardFragment(threadPath.substring(1..3))
            this.findNavController().navigate(action)
        }
    }
}
//https://stackoverflow.com/a/61768682
